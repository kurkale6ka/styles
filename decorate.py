#! /usr/bin/env python3

"""Text with styles: color, bold, ...

e.g. print text in cyan, bold, italic and underlined:
print(Text("Excepteur sint occaecat cupidatat").cyan.b.i.u)

Supported styles
----------------
Text("lorem").red
Text("lorem").green
Text("lorem").yellow
Text("lorem").blue
Text("lorem").magenta
Text("lorem").cyan
Text("lorem").res
Text("lorem").b
Text("lorem").dim
Text("lorem").i
Text("lorem").u
Text("lorem").dir # directory/ blue
Text("foreground").fg(123)
Text("background").bg(92)

It is also possible to overwrite default colors with custom ones as needed
--------------------------------------------------------------------------
Text().blue = 69 # alternate blue
Text("lorem").blue
"""

# TODO:
# Text().dir = 69, then I can refer to it as a property
# this will need def __setattr__(self, name, value: int) + eval()
#
# Properties: it should be possible to simplify the code for the
# getters/setters in order to avoid repetition

from __future__ import annotations
from collections import UserString


class Text(UserString):
    """Text is an object with:
    - a string ready for printing: \\033[32m(green) Hello \\033[0m(reset)
    - attributes: {
          styles: \\033[32m + ... if we further call _style()
          text: Hello
      }
    """

    # 4-bit colors
    _red = "\033[31m"
    _green = "\033[32m"
    _yellow = "\033[33m"
    _blue = "\033[34m"
    _magenta = "\033[35m"
    _cyan = "\033[36m"
    _res = "\033[0m"
    _b = "\033[1m"
    _dim = "\033[2m"
    _i = "\033[3m"
    _u = "\033[4m"

    # 8-bit handy colors
    _dir = "\033[38;5;69m"  # directory/ blue
    # 242: "dim"

    def __init__(self, attributes: dict | str = ""):
        # Text("Hello World").green, _style(green) returns a Text(attributes)
        if isinstance(attributes, dict):
            super().__init__(attributes["styles"] + attributes["text"] + Text._res)
        # Text("Hello World")
        else:
            super().__init__(attributes)

        self._attributes = attributes

    def _style(self, style: str) -> Text:
        """Returns a Text object so we can chain calls: Text("hi").cyan.b"""

        if isinstance(self._attributes, dict):
            styles = self._attributes["styles"] + style
            text = self._attributes["text"]
        else:
            styles = style
            text = self._attributes
        return Text(attributes={"styles": styles, "text": text})

    @property
    def red(self) -> Text:
        return self._style(Text._red)

    @red.setter
    def red(self, value: int):
        Text._red = f"\033[38;5;{value}m"

    @property
    def green(self) -> Text:
        return self._style(Text._green)

    @green.setter
    def green(self, value: int):
        Text._green = f"\033[38;5;{value}m"

    @property
    def yellow(self) -> Text:
        return self._style(Text._yellow)

    @yellow.setter
    def yellow(self, value: int):
        Text._yellow = f"\033[38;5;{value}m"

    @property
    def blue(self) -> Text:
        return self._style(Text._blue)

    @blue.setter
    def blue(self, value: int):
        Text._blue = f"\033[38;5;{value}m"

    @property
    def magenta(self) -> Text:
        return self._style(Text._magenta)

    @magenta.setter
    def magenta(self, value: int):
        Text._magenta = f"\033[38;5;{value}m"

    @property
    def cyan(self) -> Text:
        return self._style(Text._cyan)

    @cyan.setter
    def cyan(self, value: int):
        Text._cyan = f"\033[38;5;{value}m"

    @property
    def dir(self) -> Text:
        return self._style(Text._dir)

    @dir.setter
    def dir(self, value: int):
        Text._dir = f"\033[38;5;{value}m"

    # foreground, background
    def fg(self, color: int) -> Text:
        return self._style(f"\033[38;5;{color}m")

    def bg(self, color: int) -> Text:
        return self._style(f"\033[48;5;{color}m")

    # bold, dim, italic, underline
    @property
    def b(self) -> Text:
        return self._style(Text._b)

    @property
    def dim(self) -> Text:
        return self._style(Text._dim)

    @property
    def i(self) -> Text:
        return self._style(Text._i)

    @property
    def u(self) -> Text:
        return self._style(Text._u)

    def __eq__(self, other: Text) -> bool:
        if isinstance(self._attributes, dict):
            return self._attributes["text"] == other
        else:
            return self._attributes == other


if __name__ == "__main__":
    import unittest

    class TestMethods(unittest.TestCase):
        def test_eq(self):
            self.assertEqual(Text("Hello World").red, "Hello World")
            self.assertEqual(Text("Hello World").red.b, "Hello World")

    print(" No text:", Text())
    print(" No color:", Text("plain text"))
    print(" Color properties:", end=" ")
    print(Text("red").red, end=" ")
    print(Text("green").green, end=" ")
    print(Text("yellow").yellow, end=" ")
    print(Text("blue").blue, end=" ")
    print(Text("magenta").magenta, end=" ")
    print(Text("cyan").cyan)
    print(" Color fg/bg:", end=" ")
    print(Text("foreground").fg(123), end=" ")
    print(Text("background").bg(92))
    print(" Styles:", end=" ")
    print(Text("bold").red.b, end=" ")
    print(Text("dim").green.dim, end=" ")
    print(Text("italic").yellow.i, end=" ")
    print(Text("underline").blue.u, end=" ")
    print(Text("bold_i").magenta.b.i, end=" ")
    print(Text("bold_i_u").cyan.b.i.u)
    print(" Mixed:", end=" ")
    print(Text("one").cyan.u.i + "-two")
    print(" Alternate blue:", end=" ")
    Text().blue = 69
    print(Text("/etc/fstab").blue)
    print(" F-string:", end=" ")
    print(f"{Text('Hello').magenta.b.u} World {Text('!!').b}")
    print(" Justification no color:", end=" ")
    print(Text("Hello World").rjust(35, ">"))
    print(" Justification with color:", end=" ")
    print(Text("Hello World").yellow.rjust(35, ">"))
    print(" Raw:", end=" ")
    print(
        repr(Text("Hello World").red.b._attributes["styles"]).strip(  # pyright: ignore
            "'"
        )
    )

    print("\n" + Text("8-bit colors").u)

    def chunks():
        for i in range(16, 255, 6):
            yield range(i, i + 6)

    # generator of ranges
    col = chunks()

    def colors(col):
        # TODO: omit numbers
        # return ''.join('\033[48;5;{0}m    '.format(num) for num in col) + Text._res
        return "".join("\033[48;5;{0}m{0:^4}".format(num) for num in col) + Text._res

    print()
    print(colors(range(0, 8)))
    print(colors(range(8, 16)))
    print()

    row1, row2 = [], []

    for i in range(6):
        row1.append((colors(next(col)), colors(next(col)), colors(next(col))))
        row2.append((colors(next(col)), colors(next(col)), colors(next(col))))

    for r in row1:
        print(*r, sep="  ")
    print()
    for r in row2:
        print(*r, sep="  ")
    print()

    print(colors(next(col)) + colors(next(col)))
    print(colors(next(col)) + colors(next(col)))
    print()

    unittest.main()
