# Text with styles: color, bold, ...

Print text in cyan, bold, italic and underlined

```python
print(Text("Excepteur sint occaecat cupidatat").cyan.b.i.u)
```
